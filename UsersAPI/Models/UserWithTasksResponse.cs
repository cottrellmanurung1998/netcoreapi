﻿namespace UsersAPI.Models
{
    public class UserWithTasksResponse
    {
        public int PkUsersId { get; set; }
        public string Name { get; set; }
        public List<TaskDetailResponse> Tasks { get; set; }
    }

    public class TaskDetailResponse
    {
        public int PkTasksId { get; set; }
        public string TaskDetail { get; set; }
    }
}
