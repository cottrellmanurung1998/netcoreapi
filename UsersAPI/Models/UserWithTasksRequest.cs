﻿namespace UsersAPI.Models
{
    public class UserWithTasksRequest
    {
        public string Name { get; set; }
        public List<UserTask> Tasks { get; set; }
    }
}
