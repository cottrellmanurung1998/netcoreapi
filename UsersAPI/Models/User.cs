﻿namespace UsersAPI.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<UserTask> Tasks { get; set; }
    }
}
