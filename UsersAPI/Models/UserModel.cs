﻿namespace UsersAPI.Models
{
    public class UserModel
    {
        public int PkUsersId { get; set; }
        public string Name { get; set; }
        public List<TaskModel> Tasks { get; set; }
    }
}
