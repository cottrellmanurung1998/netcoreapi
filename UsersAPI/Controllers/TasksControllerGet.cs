﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using UsersAPI.Models;

[Route("api/tasks")]
[ApiController]
public class TasksControllerGet : ControllerBase
{
    // Contoh data test yang telah diinput sebelumnya
    private static readonly List<UserModel> users = new List<UserModel>
    {
        new UserModel
        {
            PkUsersId = 1,
            Name = "trainee",
            Tasks = new List<TaskModel>
            {
                new TaskModel { PkTasksId = 1, TaskDetail = "learning at coding id" },
                new TaskModel { PkTasksId = 2, TaskDetail = "complete the tasks" }
            }
        },
        new UserModel
        {
            PkUsersId = 2,
            Name = "trainer",
            Tasks = new List<TaskModel>
            {
                new TaskModel { PkTasksId = 3, TaskDetail = "teach with passion and love" }
            }
        }
    };

    [HttpGet("GetUserWithTask")]
    public IActionResult GetUserWithTask()
    {
        // Mendapatkan data yang telah diinput sebelumnya
        return Ok(users);
    }
}

/* //untuk TASKCONTROLLERSEARCH//
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using UsersAPI.Models;

[Route("api/tasks")]
[ApiController]
public class TasksControllerGetSearch : ControllerBase
{
    private static readonly List<UserModel> users = new List<UserModel>
    {
        new UserModel
        {
            PkUsersId = 1,
            Name = "trainee",
            Tasks = new List<TaskModel>
            {
                new TaskModel { PkTasksId = 1, TaskDetail = "learning at coding id" },
                new TaskModel { PkTasksId = 2, TaskDetail = "complete the tasks" }
            }
        },
        new UserModel
        {
            PkUsersId = 2,
            Name = "trainer",
            Tasks = new List<TaskModel>
            {
                new TaskModel { PkTasksId = 3, TaskDetail = "teach with passion and love" }
            }
        }
    };

    [HttpGet("GetUserWithTask")]
    public IActionResult GetUserWithTask(string name)
    {
        if (string.IsNullOrEmpty(name))
        {
            // Jika parameter name kosong, kembalikan semua data users dan tasks
            return Ok(users);
        }

        // Cari users yang memiliki nama sesuai dengan parameter name
        var filteredUsers = users.Where(u => u.Name.ToLower() == name.ToLower()).ToList();

        return Ok(filteredUsers);
    }
}
*/