﻿using Microsoft.AspNetCore.Mvc;
using UsersAPI.Models;

[Route("api/tasks")]
[ApiController]
public class TasksController : ControllerBase
{
    [HttpPost("AddUserWithTask")]
    public IActionResult AddUserWithTask(UserWithTasksRequest request)
    {
        // Get data from the request JSON
        string name = request.Name;
        List<UserTask> tasks = request.Tasks;

        // Process and save data to the database
        // You can use an ORM like Entity Framework or any other method

        // A simple example to respond with the data that is saved
        var response = new
        {
            Message = "User and Tasks added successfully",
            User = new { Name = name, Tasks = tasks }
        };

        return Ok(response);
    }
}
